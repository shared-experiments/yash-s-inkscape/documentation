# Makefile written by
# Colin Marquardt <colin@marquardt-home.de>, 2007.

# This Makefile stub is meant to be included from a tutorial subdirectory,
# therefore we need the ".." in some places.

# some tool definitions
XML2PO       := xml2po
INKSCAPEPATH := inkscape

# use a clean profile for Inkscape to minimize side-effects
export INKSCAPE_PROFILE_DIR=$(CURDIR)/../.inkscape_profile_dir

# the current path (which will be inside a tutorial directory)
# with the slashes replaced by spaces
PATHELEMS := $(subst /, ,$(PWD))
# the last element of the path with spaces is the tutorial name
TUTORIAL := $(word $(words $(PATHELEMS)), $(PATHELEMS))

# the existing po files are used to determine which HTML and SVG files to build
SRC := $(wildcard *.po)

# all languages for which a po file has been found
ifdef MYLANG
MYLANGS := $(MYLANG)
else
MYLANGS := $(SRC:.po=)
endif

ifdef MYLANG
TARGETS_HTML := tutorial-$(TUTORIAL).$(MYLANG).html
TARGETS_SVG := tutorial-$(TUTORIAL).$(MYLANG).svg
else
# generate filenames for HTML and SVG tutorials
# Example: "tutorial-shapes. + de.html" --> tutorial-shapes.de.html
TARGETS_HTML := $(addprefix tutorial-$(TUTORIAL)., $(SRC:.po=.html))
# Example: "tutorial-shapes. + de.svg" --> tutorial-shapes.de.svg
TARGETS_SVG  += $(addprefix tutorial-$(TUTORIAL)., $(SRC:.po=.svg))
endif

.PHONY: all
all: html svg

.PHONY: html
html: $(TARGETS_HTML)

.PHONY: svg
svg: $(TARGETS_SVG)

# FIXME: make targets dependent on Makefile itself

tutorial-$(TUTORIAL).%.html: tutorial-$(TUTORIAL).%.xml ../tutorial-html.xsl ../make-html images-%
# Example: ./make-html basic /usr/bin/inkscape es
	cd .. && ./make-html $(TUTORIAL) $(INKSCAPEPATH) $*

tutorial-$(TUTORIAL).%.svg: tutorial-$(TUTORIAL).%.xml ../tutorial-svg.xsl ../make-svg images-%
	cd .. && ./make-svg $(TUTORIAL) $(INKSCAPEPATH) $*

# generate the localized XML version of the tutorial:
.PRECIOUS: tutorial-$(TUTORIAL).%.xml  # keep the translated XML files around (no need to delete)
tutorial-$(TUTORIAL).%.xml: %.po tutorial-$(TUTORIAL).xml
	$(XML2PO) --po-file=$< tutorial-$(TUTORIAL).xml > $@

# find translatable images
ALL_IMAGES := $(wildcard $(TUTORIAL)-f*.svg)
TRANSLATABLE_IMAGES := $(shell grep -il $(TUTORIAL)-f??.svg -e '<text\|<tspan\|<flowroot' | cut -f 1 -d '.')

# create localized versions of all translatable images
# Example: make images-de
.PHONY: images-%
images-%: $(addsuffix -%.svg, $(TRANSLATABLE_IMAGES)) ;

# Generate implicit rules (one target per untranslated file)
# to create localized versions for all translatable images.
#   will result in targets like
#     shapes-f01-%.svg: %.po
#   which can be called with
#     make shapes-f01-de.svg
define GEN_IMAGE_RULE
.PRECIOUS: $(1)-%.svg  # keep the translated images around (no need to delete)
$(1)-%.svg: $(1).svg %.po
	$$(XML2PO) --po-file=$$*.po $$< > $$@

	@# these are some manual hacks to get localized PNGs included in SVGs working
	@# TODO: check if this can be simplified / generalized
	@if [ '$(TUTORIAL)' = 'tracing' ] && [ -f potrace.$$*.png ] && grep -q 'potrace.png' $$@; then \
		sed -e "s/potrace.png/potrace.$$*.png/g" $$@ > $$@.temp.svg && mv $$@.temp.svg $$@; \
	fi
	@if [ '$(TUTORIAL)' = 'tracing-pixelart' ] && [ -f pixelart-dialog.$$*.png ] && grep -q 'pixelart-dialog.png' $$@; then \
		sed -e "s/pixelart-dialog.png/pixelart-dialog.$$*.png/g" $$@ > $$@.temp.svg && mv $$@.temp.svg $$@; \
	fi
endef
$(foreach IMAGE,$(TRANSLATABLE_IMAGES),$(eval $(call GEN_IMAGE_RULE,$(IMAGE))))

# target to scour SVG files for usage as figures in HTML
scoured/%.svg: %.svg
	@mkdir -p scoured
	scour -i $< -o $@ --disable-embed-rasters --remove-metadata --enable-id-stripping --shorten-ids

# scour all SVG figures
.PHONY: scour-all-figures
scour-all-figures: $(addprefix scoured/, $(ALL_IMAGES)) ;



# rules for (re-)creating .pot and .po files
.PHONY: pot
pot: tutorial-$(TUTORIAL).pot

tutorial-$(TUTORIAL).pot: tutorial-$(TUTORIAL).xml $(sort $(wildcard $(TUTORIAL)-f*.svg))
	$(XML2PO) --output=$@ $^

.PHONY: po
po: *.po

%.po: tutorial-$(TUTORIAL).pot
	msgmerge $*.po tutorial-$(TUTORIAL).pot > $*.new.po
	@mv $*.new.po $*.po



# This target is not as effective as it could be since xml2po cleverly "repairs"
# certain wrong markup
.PHONY: check
check: tutorial-$(TUTORIAL).$(MYLANG).xml $(MYLANG).po check_mylang
	xmllint --valid --noout tutorial-$(TUTORIAL).$(MYLANG).xml

.PHONY: copy
copy: copy-svg copy-html

.PHONY: copy-html
copy-html: scour-all-figures
#	@echo ""
#	@echo "In order to copy the HTML versions of the tutorials to the directory where Inkscape can find them, try:"
	mkdir -p ../../export-website/tutorials/$(TUTORIAL)
	cp *.html ../../export-website/tutorials/$(TUTORIAL)/
	mv ../../export-website/tutorials/$(TUTORIAL)/tutorial-$(TUTORIAL).en.html ../../export-website/tutorials/$(TUTORIAL)/tutorial-$(TUTORIAL).html
	cp scoured/*.svg ../../export-website/tutorials/$(TUTORIAL)/
	cp *.png ../../export-website/tutorials/$(TUTORIAL)/ || true
	cp *.jpg ../../export-website/tutorials/$(TUTORIAL)/ || true
	cp ../_tutorial-header.svg ../../export-website/tutorials/
	cp ../_tutorial-footer.svg ../../export-website/tutorials/
	cp ../tutorial-html.css   ../../export-website/tutorials/
	cp ../mouse.svg           ../../export-website/tutorials/

.PHONY: copy-svg
copy-svg:
#	@echo ""
#	@echo "In order to copy the SVG versions of the tutorials to the directory where Inkscape can find them, try:"
	mkdir -p ../../export-inkscape/tutorials/
	cp tutorial-$(TUTORIAL).*.svg ../../export-inkscape/tutorials/
	mv ../../export-inkscape/tutorials/tutorial-$(TUTORIAL).en.svg ../../export-inkscape/tutorials/tutorial-$(TUTORIAL).svg
	cp *.png ../../export-inkscape/tutorials/ || true
	cp *.jpg ../../export-inkscape/tutorials/ || true

.PHONY: clean
clean: clean-html clean-svg
	rm -rf $(CURDIR)/../.inkscape_profile_dir

.PHONY: clean-html
clean-html: clean-xml
	rm -f *.html
	rm -rf scoured

.PHONY: clean-svg
clean-svg: clean-xml
	rm -f tutorial-$(TUTORIAL)*.svg
	rm -f $(TUTORIAL)-*-*.svg

.PHONY: clean-xml
clean-xml:
	rm -f tutorial-$(TUTORIAL).*.xml

.PHONY: help
help:
	@echo "Targets:"
	@echo "   all [MYLANG=<LANG>]     - Create HTML and SVG files (can be restricted to <LANG>)"
	@echo "   html [MYLANG=<LANG>]    - Create HTML files (can be restricted to <LANG>)"
	@echo "   svg [MYLANG=<LANG>]     - Create SVG files (can be restricted to <LANG>). You will want to use the 'images' target before this."
	@echo "   pot                     - Update tutorial-*.pot"
	@echo "   po                      - Update tutorial-*.pot and all po files"
	@echo "   check MYLANG=<LANG>     - Run xmllint on the generated XML file for language <LANG>"
	@echo "   copy-svg                - Instructions for copying generated SVG tutorials to the correct directory"
	@echo "   copy-html               - Instructions for copying generated HTML tutorials to the correct directory"
	@echo "   copy                    - Instructions for copying both SVG and HTML tutorials"
	@echo "   clean                   - Clean all generated files"
	@echo "   clean-svg               - Instructions for cleaning the SVG files in the tutorial directory"
	@echo "   clean-html              - Instructions for cleaning the HTML files in the tutorial directory"

.PHONY: check_mylang
check_mylang:
ifndef MYLANG
	@echo "E:Syntax error - you didn't define MYLANG."
	@echo ""
	@$(MAKE) -s help
	@exit 1
else
	@if [ ! -r $(MYLANG).po ]; then \
	echo "E:Language file '$(MYLANG).po' does not exist!"; \
	echo ""; \
	exit 1; \
	fi
endif

# TODO: check pofilter from translate-toolkit
# TODO: make update-po work on all languages
