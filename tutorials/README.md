Tutorials
=========

This is where tutorial sources are kept in [DocBook 4.5](https://tdg.docbook.org/tdg/4.5/docbook.html) format
(with SVG illustrations).

Creating new tutorials
----------------------

Place each tutorial and its illustrations into a subdirectory. An example is
basic/ with the Basic Tutorial.

Copy and use `template.xml` as skeleton to creating new tutorial source.
You find supported DocBook tags in this template. Please use unly this tags and
preserve structure. You can validate XML width `xmllint --valid your-tutorial.xml`

Create figures with provided template (`template-figure.svg`). Please
preserve canvas width (300px) and update canvas height to the content without margins
on the top and bottom. For consistent look please align drawing horizontally to the center
on the canvas. (It's important for HTML output to use `viewBox`). If you want to add (translatable)
labels follow the example.


Bulding tutorials
-----------------

If you want to build a tutorial then you can use the scripts `make-html`,
`make-svg`, and `make-all`. The usage instructions and requirements are in these
files.
