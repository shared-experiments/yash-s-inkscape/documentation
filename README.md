Inkscape Documentation
======================

This includes all files required for creating localized documentation for Inkscape.

At the time of this writing this includes:
 - Inkscape man pages (*man*)
 - Inkscape keyboard and mouse reference (*keys*)
 - Inkscape tutorials (*tutorials*)

It also has rules to create translation statistics (*statistics*).

Translating Documentation in this repository
--------------------------------------------

**What needs translation?**

An overview about the status of translations in the Inkscape project is available at:

 - https://inkscape.org/doc/devel/translations-statistics-master.html (for Inkscape master branch)
 - https://inkscape.org/doc/devel/translations-statistics-092.html (for the 0.92.x series)

**How do I get the files?**

If you feel comfortable using git and/or the GitLab webinterface, the preferred way is to fork this repository and work on a new branch in this fork.

Alternatively translators can download the files from this repository as a zip archive and translate the documentation po files for their language.
At a later point, we may add info here about different versions that need to be translated.

For non-programmers, the quickest option to obtain the files is to download the [zip archive](https://gitlab.com/inkscape/inkscape-docs/documentation/repository/0.92.x/archive.zip).

**Which files need to be translated?**

You can find po files ready for translation in the following subdirectories:
 - keys
 - man
 - tutorials/\<various tutorial subdirectories\>

If there isn't a file for your language yet, you can create a new one by copying the files 'keys.pot', 'man.pot' or '\<tutorial_name.pot\>' and renaming the copy to '<your_language_code>.po'.

**Is there anything important I need to know?**

 - The translation files for the manual contain some unusual formatting, e.g. <pre>C\<E\<lt\>defsE\<gt\>\></pre> Please leave these strings intact.
 - For the tutorials, we also need translations of the header and footer SVGs and for some of the screenshot PNG files. 
   To find these files, watch out for SVG and PNG files that have a file name that only differs by language codes.
   Some strings in the header SVG file are paths, not editable texts — this is to ensure that the text will render correctly to the end user.
   To translate it, you will have to recreate the text object yourself (use a generic sans-serif font with appropriate license, e.g. ‘DejaVu Sans’ or ‘Bistream Vera Sans’, in italic)
   and convert it to a path when you're done. Also consider the translucent ‘tutorial’ text path in the background.

**I'm ready. How do I submit my translations?**

The preferred way is to create a merge request using the GitLab webinterface.

Alternatively you can create a new new issue (https://gitlab.com/inkscape/inkscape-docs/documentation/issues) and attach your edited file.
You may add the tags 'documentation' and 'translation' to it.

If you need help with the process feel free to contact us in our chat room at https://chat.inkscape.org/channel/documentation


Creating documentation
----------------------

If using Ubuntu, you need to install (at least) the packages `gnome-doc-utils` and `python-pexpect`.

Each of the subdirectories has a README with detailed information on how to use (including more info on software requirements).

Also you'll find a Makefile in each subdirectory for usage with GNU make (use `make help` for usage information)

If you're feeling lucky (or know that you have all requirements installed)
you can also use the convenience targets from the top level Makefile:
 - use `make` to generate all documentation
 - use `make help` to see additional usage information

*Note: As git does not track file modification times `make` might not be able to determine which targets need to be
 re-made after checking out new files. In order to re-make all files use `make --always-make` (or `make -B`).*